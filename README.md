# README

Cette application web est développée sur RubyOnRails en utilisant PostgreSQL comme SGBD. 

Aussi publiée sur Heroku https://agorizeskills.herokuapp.com/

## Information technique:

* Ruby version : ruby '2.3.1'

* Rails version : 'rails', '~> 5.2.3'

* DB: PostgreSQL RDBMS

## Commands d'execution:

~~~sh
bundle

rails db:drop db:create db:migrate db:seed

rails s
~~~

## Command de test:
~~~
rails test (in the root directory)
~~~

-----------------------------------------------------------------------------------------------------

# Test technique Agorize

## Question 1 :

Étant donné une table SKILLS.
Un skill peut être l'enfant ou le parent d'un autre skill.
Un enfant ne peut avoir qu'un seul parent.
Un enfant ne peut pas avoir d'enfant.

Par exemple, dans cette table, Foot est un enfant de Football, idem pour Soccer.

Essayer d'obtenir :
* Pour chaque skill parent la somme totale des points obtenus par les utilisateurs associés à ce
skill ou à ses enfants.

* Ajouter à la requête précédente pour chaque skill parent le nombre total d'utilisateurs
associés à ce parent ou à ses enfants

~~~
SKILLS
+-----------------------+
|ID|NAME      |PARENT_ID|
+-----------------------+
|1 |Football  |         |
+-----------------------+
|2 |Basketball|         |
+-----------------------+
|3 |Foot      |1        |
+-----------------------+
|4 |Basket    |2        |
+-----------------------+
|5 |Socker    |1        |
+-----------------------+
~~~

~~~
SKILLS_USERS
+-------------------+
|ID|SKILL_ID|USER_ID|
+-------------------+
|1 |1       |1      | 
+-------------------+
|2 |1       |2      | 
+-------------------+
|3 |3       |3      | 
+-------------------+
|4 |2       |4      | 
+-------------------+
|5 |5       |5      |
+-------------------+
~~~
~~~
USERS
+---------+
|ID|POINTS|
+---------+
|1 |100   |
+---------+
|2 |200   |
+---------+
|3 |100   |
+---------+
|4 |50    |
+---------+
|5 |10    |
+---------+
~~~

Résultat attendu (en une requête qu'il faudra nous fournir) :

~~~
+--------------------------------+
|ID|NAME      |POINTS|USERS_COUNT|
+--------------------------------+
|1 |Football  |410   |4          |
+--------------------------------+
|2 |Basketball|50    |1          |
+--------------------------------+
~~~
## Question 2 :

Créer une application Ruby on Rails implémentant ce modèle de données.
Créer les modèles et controlleurs nécessaires afin de pouvoir:

* créer des utilisateurs
* créer des compétences
* associer une compétence à un ou plusieurs utilisateurs.

Enfin, afficher sur une page un tableau présentant les informations obtenues lors de la question 1.
L'application contiendra un seed permettant d'avoir un minimum de données.

Une bonne application est une application testée.




# Réponses

## Question 1 :

Les tables Users, Skills, Skills_Users sont crées. 
Les modèles de données et ses méthodes garantissent les conditions pour les relations.


Pour obtenir le résultat suivant avec les données du seed : 

~~~
+--------------------------------+
|ID|NAME      |POINTS|USERS_COUNT|
+--------------------------------+
|1 |Football  |410   |4          |
+--------------------------------+
|2 |Basketball|50    |1          |
+--------------------------------+
~~~

La requête SQL est :

~~~sql
SELECT skills.name AS Name,
       SUM(users.points) + COALESCE(parent.parent_points, 0) AS Points,
       COUNT(users.id) + COALESCE(parent.parent_count, 0) AS N_Users
FROM skills
    LEFT OUTER JOIN
    (
        SELECT SUM(users2.points) AS parent_points,
               COUNT(users2.id) AS parent_count,
               skills2.parent_skill_id AS parent_skill_id
        FROM skills skills2
            INNER JOIN skills_users skills_users2
                ON skills2.id = skills_users2.skill_id
            INNER JOIN users users2
                ON users2.id = skills_users2.user_id
        GROUP BY skills2.parent_skill_id
        HAVING skills2.parent_skill_id IS NOT NULL
    ) parent
        ON skills.id = parent.parent_skill_id
    INNER JOIN skills_users
        ON skills_users.skill_id = skills.id
    INNER JOIN users
        ON users.id = skills_users.user_id
GROUP BY skills.name,
         skills.parent_skill_id,
         parent.parent_points,
         parent.parent_count
HAVING skills.parent_skill_id IS NULL;
~~~

## Question 2 :
* Un seed avec les données des exemples est fourni pour tester l'application. 
* L'application permet de créer des nouveaux utilisateurs, nouvelles compétences et les associer.
* Il est possible d'effectuer les opérations CRUD sur les éléments des trois entités. 
* Tous les formulaires gèrent possibles données incorrectes saisisses.
* Des tests sont écrits pour les contrôleurs et le modèles de l’application. L’exécution de 
tests a les résultats suivants :
~~~
39 runs, 59 assertions, 1 failures, 0 errors, 0 skips
~~~