class SkillsUser < ApplicationRecord
  belongs_to :skill
  belongs_to :user
  validates :skill_id, presence: true, numericality: { only_integer: true }
  validates :user_id, presence: true, numericality: { only_integer: true }
  validates :user, uniqueness: {scope: :skill}
end
