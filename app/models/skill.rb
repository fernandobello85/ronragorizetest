class Skill < ApplicationRecord
  belongs_to :parent_skill, class_name: 'Skill', 
              foreign_key: 'parent_skill_id', 
              optional: true
  has_many :children_skill, class_name: 'Skill', 
            foreign_key: 'parent_skill_id', dependent: :nullify
  has_many :skills_users, dependent: :destroy
  has_many :users, through: :skills_users
  validates :name, presence: true
  validates :name, uniqueness: true
  validate :is_parent?
  validate :parent_is_parent?, :if => :data_valid?
  scope :get_parents, -> {where(parent_skill_id: nil)}

  def number_of_users
  	users.length
  end

  def number_of_points
  	users.sum(&:points)
  end
  def data_valid?
    parent_skill_id? && name?
  end
  def is_parent?
  	errors.add(:base, 'The skill is already a parent skill') if children_skill.any?
  end
  def parent_is_parent?
    if parent_skill.present?
      errors.add(:base, 'The parent skill is already a child') if parent_skill.parent_skill_id?
    end
  end

end
