class User < ApplicationRecord
	has_many :skills_users, dependent: :destroy
	has_many :skills, through: :skills_users, dependent: :nullify
	validates :points, presence: true
	validates :points, :numericality => { :greater_than_or_equal_to => 0 }
end
