module SkillsHelper
	def self.consolidate 
		Skill.includes(:users).get_parents.map do |skill|
			{
				name: skill.name,
				number_of_points: skill.number_of_points + 
								skill.children_skill.includes(:users).sum(&:number_of_points),
				number_of_users: skill.number_of_users + 
							skill.children_skill.includes(:users).sum(&:number_of_users)
			}
		end
	end
end
