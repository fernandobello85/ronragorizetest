require 'test_helper'

class SkillTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "test valid" do
    skill = Skill.new(name: "Voleyball", parent_skill_id: 99)
    assert_equal "Voleyball", skill.name
    assert_equal 99, skill.parent_skill_id
  end
  test "test without arguments" do
    skill = Skill.new(name: nil, parent_skill_id: nil)
    refute skill.valid?
    assert_not_nil skill.errors[:name]
  end
end
