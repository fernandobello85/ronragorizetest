require 'test_helper'

class SkillsUserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "test valid" do
    skills_user = SkillsUser.new(skill_id: 11, user_id: 99)
    assert_equal 11, skills_user.skill_id
    assert_equal 99, skills_user.user_id
  end
  test "test without arguments" do
    skills_user = SkillsUser.new(skill_id: nil, user_id: nil)
    refute skills_user.valid?
    assert_not_nil skills_user.errors[:skill_id]
    assert_not_nil skills_user.errors[:user_id]
  end
end
