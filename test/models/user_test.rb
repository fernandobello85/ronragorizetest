require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "test points > 0" do
    user = User.new(points: 20)
    assert_equal 20, user.points
  end
  test "test points < 0" do
    user = User.new(points: -20)
    refute user.valid?
    assert_not_nil user.errors[:points]
  end
end
