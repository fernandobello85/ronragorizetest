require "application_system_test_case"

class SkillsUsersTest < ApplicationSystemTestCase
  setup do
    @skills_user = skills_users(:one)
  end

  test "visiting the index" do
    visit skills_users_url
    assert_selector "h1", text: "Skills Users"
  end

  test "creating a Skills user" do
    visit skills_users_url
    click_on "New Skills User"

    fill_in "Skill", with: @skills_user.skill_id
    fill_in "User", with: @skills_user.user_id
    click_on "Create Skills user"

    assert_text "Skills user was successfully created"
    click_on "Back"
  end

  test "updating a Skills user" do
    visit skills_users_url
    click_on "Edit", match: :first

    fill_in "Skill", with: @skills_user.skill_id
    fill_in "User", with: @skills_user.user_id
    click_on "Update Skills user"

    assert_text "Skills user was successfully updated"
    click_on "Back"
  end

  test "destroying a Skills user" do
    visit skills_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Skills user was successfully destroyed"
  end
end
