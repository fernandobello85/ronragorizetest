require 'test_helper'

class SkillsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @skill = skills(:one)
  end

  test "should get index" do
    get skills_url
    assert_response :success
  end

  test "should get new" do
    get new_skill_url
    assert_response :success
  end

  test "should create skill" do
    assert_difference('Skill.count') do
      post skills_url, params: { skill: { name: 'Plop' } }
    end

    assert_redirected_to skill_url(Skill.last)
    assert_equal 'Skill was successfully created.', flash[:notice]
  end

  test "should show skill" do
    get skill_url(@skill)
    assert_response :success
  end

  test "should get edit" do
    get edit_skill_url(@skill)
    assert_response :success
  end

  test "should update skill" do
    patch skill_url(@skill), params: { skill: { name: @skill.name, parent_skill_id: @skill.parent_skill_id } }
    assert_response :success
  end

  test "should destroy skill" do
    assert_difference('Skill.count', -1) do
      delete skill_url(@skill)
    end

    assert_redirected_to skills_url
  end
  test "should not create skill without name" do
    assert_no_difference('Skill.count') do
      post skills_url, params: { skill: { name: nil } }
    end

    assert_raise(Exception) { whatever.merge }
  end
  test "should not create skill with repeated name" do
    assert_no_difference('Skill.count') do
      post skills_url, params: { skill: { name: @skill.name } }
    end

    assert_raise(Exception) { whatever.merge }
  end
  test "should not create skill if parent is already a child" do
    assert_no_difference('Skill.count') do
      post skills_url, params: { skill: { name: 'Plop', parent_skill_id: @skill.parent_skill_id } }
    end

    assert_raise(Exception) { whatever.merge }
  end
  test "should not update skill with repeated name" do
    patch skill_url(@skill), params: { skill: { name: @skill.name } }
    assert_raise(Exception) { whatever.merge }
  end
  test "should not update skill if parent is already a child" do
    patch skill_url(@skill), params: { skill: { name: 'Plop', parent_skill_id: @skill.parent_skill_id } }
    assert_raise(Exception) { whatever.merge }
  end
end
