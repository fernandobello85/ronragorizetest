# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


#Seed for the users
Iron_Man = User.create(points: 100)
Wasp = User.create(points: 200)
Hulk = User.create(points: 100)
Captain_America = User.create(points: 50)
Black_Widow = User.create(points: 10)

#Seed for the sports
football = Skill.create(name: 'Football')
basketball = Skill.create(name: 'Basketball') 

foot = Skill.create(name: 'Foot', parent_skill_id: football.id)
basket = Skill.create(name: 'Basket', parent_skill_id: basketball.id)
soccer = Skill.create(name: 'Soccer', parent_skill_id: football.id)

#Seed for the associations 
SkillsUser.create(user: Iron_Man, skill: football)
SkillsUser.create(user: Wasp, skill: football)
SkillsUser.create(user: Hulk, skill: foot)
SkillsUser.create(user: Captain_America, skill: basketball)
SkillsUser.create(user: Black_Widow, skill: soccer)
